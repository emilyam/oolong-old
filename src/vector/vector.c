/* * * * * * * * * * * * * * * * * * * *
 * vector.h                            *
 * a simple vector implementation in C *
 *                                     *
 * (c) 2018 Emily Alice Michael        *
 * Released under the MIT license      *
 * * * * * * * * * * * * * * * * * * * */
#include <string.h>
#include <stdlib.h>
#include "vector.h"

const int VECTORMAXSIZE = 0x40000000;
const int VECTORMINSIZE = 4;

/* rounds n up to next power of 2
 * up to 2^30
 * this function is no longer used,
 * but I'm keeping it here so I remember the algorithm
 * in case I ever need it again
 */
/*
int pow2round(int n)
{
    if(n <= 0)
        return 0;
    if(n == 1)
        return 1;
    if(n >= VECTORMAXSIZE)
        return VECTORMAXSIZE;
    int temp = --n;
    do {
        n = temp;
        temp &= n - 1;
    } while (temp);
    return n << 1;
}
*/

Vector vnew()
{
    Vector newvector = malloc(sizeof(struct vector));
    newvector->length = VECTORMINSIZE;
    newvector->current = -1;
    newvector->data = malloc(VECTORMINSIZE * sizeof(void*));
    return newvector;
}

int vadd(Vector v, void *datum)
{
    ++v->current;
    if(v->current == v->length)
    {
        if(v->length == VECTORMAXSIZE)
        {
            --v->current;
            return -1;
        }
        v->length *= 2;
        v->data = realloc(v->data, sizeof(void *) * v->length);
    }
    v->data[v->current] = datum;
    return v->current;
}

void vremove(Vector v, int index)
{
    if(index < 0 || index >= v->current)
        return;
    --v->current;
    memcpy(v->data + index, v->data + index + 1, v->length - index);
    if(v->current << 1 < v->length && v->length > VECTORMINSIZE)
    {
        v->length /= 2;
        v->data = realloc(v->data, sizeof(void *) * v->length);
    }
}

void* vget(Vector v, int index)
{
    if(index < 0 || index > v->current)
        return NULL;
    return v->data[index];
}

void vdelete(Vector v)
{
    free(v->data);
    free(v);
}

void vdeleteall(Vector v)
{
    for(int i = 0; i <= v->current; ++i)
        free(v->data[i]);
    vdelete(v);
}
