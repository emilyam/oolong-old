/* * * * * * * * * * * * * * * * * * * *
 * vector.h                            *
 * a simple vector implementation in C *
 *                                     *
 * (c) 2018 Emily Alice Michael        *
 * Released under the MIT license      *
 * * * * * * * * * * * * * * * * * * * */

struct vector
{
    void **data;
    int length;
    int current;
};
typedef struct vector* Vector;

/* Make new vector of at least length len
 * and return pointer that new vector
 * returns null if failed
 */
Vector vnew();

/* Add datum to vector;
 * return index of datum
 */
int vadd(Vector v, void *datum);

/* Remove datum at index
 */
void vremove(Vector v, int index);
/* Get datum at index
 */
void* vget(Vector v, int index);
/* Deletes vector. Does not free any
 * pointers in the vector!
 */
void vdelete(Vector vector);
/* Deletes vector and frees all data
 * Beware what your data points to!
 */
void vdeleteall(Vector v);
