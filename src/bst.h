typedef struct treenode
{
    terminal *t;
    struct treenode *left;
    struct treenode *right;
} Treenode;
