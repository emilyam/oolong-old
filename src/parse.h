/*
 * parse.h
 */
#include "vector/vector.h"
#include "bst.h"

typedef struct terminal
{
    char *text;
    enum tokentype { ERR, OP, NUM} type;
} Terminal;

/* parse input string
 * calls split() then decide()
 * returns vector of terminal*
 */
Vector parse(char *s);
/* words is vector of char*
 * return type is vector of terminal*
 * decide type of each terminal
 */
Vector decide(Vector words);
/* split string at characters in DELIM */
Vector split(char *s);
/* Build a decision tree from a vector of terminals
 * returns the head of that tree
 */
Treenode buildtree(Vector v);

/* Decide if string is a terminal of that type */
int isOP(char *s);
int isNUM(char *s);
