#include <stdio.h>
#include <string.h>

static char input[2048];

int main(int argc, char **argv)
{
    puts("Enter \"exit\" to exit.\n");
    while (strncmp(input, "exit", 4))
    {
        fputs("> ", stdout);
        fgets(input, 2048, stdin);
        puts(input);
    }

    return 0;
}
