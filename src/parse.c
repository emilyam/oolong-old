#include <assert.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include "parse.h"

const char DELIM[4] = " \t\n";

Vector parse(char *s)
{
    assert(s != NULL);
    Vector tokens = split(s);
    Vector terminals = decide(tokens);
    vdelete(tokens);
    return terminals;
}
Vector decide(Vector v)
{
    assert(v != NULL);
    Vector vterm = vnew();
    for(int i = 0; i <= v->current; ++i)
    {
        char *s = vget(v,i);
        struct terminal *t = malloc(sizeof(struct terminal));
        if(isOP(s))
            t->type = OP;
        else if(isNUM(s))
            t->type = NUM;
        else
            t->type = ERR;
        t->text = s;
        vadd(vterm, t);
    }
    return vterm;
}

Vector split(char *s)
{
    assert(s != NULL);
    Vector tokens = vnew();
    char *tk = strtok(s, DELIM);
    if(tk == NULL)
    {
        vdelete(tokens);
        tokens = NULL;
    }
    else while(tk != NULL)
    {
        vadd(tokens, tk);
        tk = strtok(NULL, DELIM);
    }

    return tokens;
}

Treenode buildtree(Vector v)
{
    Treenode *t;
    return buildtreerecursive(v, 0, t);
}
int buildrecursive(Vector v, int index, Treenode *t)
{
}

int isOP(char *s)
{
    if(!strncmp(s, "+", 2))
        return 1;
    if(!strncmp(s, "-", 2))
        return 1;
    if(!strncmp(s, "*", 2))
        return 1;
    if(!strncmp(s, "/", 2))
        return 1;
    return 0;
}
int isNUM(char *s)
{
    int len = strlen(s);
    for(int i = 0; i < len; ++i)
        if(!isdigit(s[i]))
            return 0;
    return 1;
}

