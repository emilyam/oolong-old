CC=gcc
FLAGS=-std=c99 -g -Wall -Wextra -pedantic

repl: src/repl.c
	$(CC) -o $@ $< $(FLAGS)
