# Oolong

This is a simple functional language inspired by Lisp, Haskell, and C. Yikes.

## Plan

### Grammar

Specify a context-free grammar for the language. This step is mostly complete.

### Parser

Write a parser in C that unambiguously parses Oolong code into a tree.

### Compiler

Write a compiler in C that compiles Oolong into assembly.

### Native compiler

Write a compiler in Oolong that compiles itself.
